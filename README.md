# CSS Library

A collection of useful css snippets and modules using [BEM naming conventions](http://getbem.com/naming/).

## Index
* [Anchor Link Highlighting and Offset](#markdown-header-anchor-link-highlighting)
* [Box Shadows](#markdown-header-box-shadow)
* [CSS Variables Ponyfill](#markdown-header-css-vars-ponyfill)

## Anchor Link Highlighting
Highlights targeted anchor links upon click. Includes support for offset if there is a sticky header.

## Box Shadow
Default box-shadows set up as css variables.

## Css Vars Ponyfill

Includes css-vars-ponyfill for old browser supoort of css vars. See documentation of the ponyfill here: https://github.com/jhildenbiddle/css-vars-ponyfill

CSS Vars support info at caniuse: https://caniuse.com/#feat=css-variables
